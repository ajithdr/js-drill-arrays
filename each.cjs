const items = [1, 2, 3, 4, 5, 5];

function cb(item, index) {
    console.log(item);
}

function each(elements, cb) {

    if(!Array.isArray(elements)) {
        return;
    }

    for(let index = 0; index < elements.length; index++) {
        cb(elements[index], index);
    }
}

// each(items, cb);
module.exports = each;