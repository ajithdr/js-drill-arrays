const items = [1, 2, 3, 4, 5, 5];

function cb(item, index, arr) {
    return item % 2 == 0;
}

function filter(elements, cb) {
    const evenArr = [];

    for(let index = 0; index < elements.length; index++) {
        if(cb(elements[index], index, elements) === true) {
            evenArr.push(elements[index]);
        }
    }

    return evenArr;
}

// const result = filter(items, cb);
// console.log(result);

module.exports = filter;