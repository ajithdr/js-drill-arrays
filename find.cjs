const items = [1, 2, 3, 4, 5, 5];

function cb(item, targetItem) {
    return item === targetItem;
}

function find(elements, cb, targetElement) {
    
    for(let index = 0; index < elements.length; index++) {
        if(cb(elements[index], targetElement)) {
            return targetElement;
        }
    }

    return undefined;
}   

// const exists = find(items, cb, 5);
// console.log(exists);

module.exports = find;