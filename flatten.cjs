const nestedArray = [1, undefined, [2], [[3]], [[[4]]]];

let flatArray = [];

function flatten(elements, depth=1) {

    if(depth < 0){
        return elements;
    }

    if(elements === undefined || elements === '') {
        return;
    }

    for(let index = 0; index < elements.length; index++) {

        if(typeof elements[index] === 'number' || typeof elements[index] === 'string' || depth === 0) {
            flatArray.push(elements[index]);
            continue;
        }

        if(depth == 'Infinity' || depth > 0) {
            flatten(elements[index], depth-1);
        } else {
            flatArray.push(elements[index]);
            return;
        }
    }

    return flatArray;
}


// console.log(flatten([], 0));
// console.log([].flat(0));

module.exports = flatten;