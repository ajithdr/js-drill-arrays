const items = [1, 2, 3, 4, 5, 5];

function cb(item, index, arr) {
    return item*item;
}

function map(elements, cb) {

    if(!Array.isArray(elements)) {
        return [];
    }

    const squares = []
    for(let index = 0; index < elements.length; index++) {
        let square;

        if(Array.isArray(elements)) {
            square = cb(elements[index], index, elements);
        } else {
            square = cb({key, value}, index, elements);
        }
        squares.push(square);
    }

    return squares;
}

// const squares = map(items, cb);
// console.log(squares);

module.exports = map;