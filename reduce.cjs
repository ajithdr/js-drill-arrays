const items = [1, 2, 3, 4, 5, 5];

function cb(accumulator, currentValue, index, arr) {
    return accumulator + currentValue;
}

function reduce(elements, cb, startingValue) {

    if(elements.length === 0) {
        if(startingValue !== undefined) {
            return startingValue;
        } else {
            return 0;
        }
    }

    let accumulator = startingValue, startIndex = 0;
    if(startingValue === undefined) {
        accumulator = elements[0];
        startIndex = 1;
    } 

    for(let index = startIndex; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements);
    }

    return accumulator;
}

const sum = reduce(items, cb, 0);
console.log(sum);

module.exports = reduce;