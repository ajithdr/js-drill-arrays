const { each, cb } = require('../each.cjs');
const items = [1, 2, 3, 4, 5];

test('Each function', () => {
  expect(each(items, cb)).toStrictEqual(items.forEach(item => console.log(item)));  
})