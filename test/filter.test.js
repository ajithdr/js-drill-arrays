const { filter, cb } = require('../filter.cjs');
const items = [1, 2, 3, 4, 5];

test('Filter function', () => {
  expect(filter(items, cb)).toStrictEqual(items.filter(item => item % 2 == 0));  
})