const { find, cb } = require('../find.cjs');
const items = [1, 2, 3, 4, 5];

test('Find function', () => {
  expect(find(items, cb, 5)).toStrictEqual(items.find(item => item === 5));  
})