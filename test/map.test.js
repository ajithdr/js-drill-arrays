const  { map, cb } = require('../map.cjs');
const items = [1, 2, 3, 4, 5];

test('Map function', () => {
  expect(map(items, cb)).toStrictEqual(items.map(item => item*item));  
})