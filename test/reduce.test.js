const { reduce, cb } = require('../reduce.cjs');
const items = [1, 2, 3, 4, 5];

const expectedResult = items.reduce((acc, cur) => {
    return acc + cur;
}, 0)

test('Reduce function', () => {
  expect(reduce(items, cb, 0)).toStrictEqual(expectedResult);  
})